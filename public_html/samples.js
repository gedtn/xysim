
var testBalls2 = function(myCanvas, mySimulation) {
    
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    mySimulation.setTimeStep(30);

    var base_colour = new myApp.utils.colour().setHex('#000000');
    var end_colour = new myApp.utils.colour().setHex('#FFFFFF');
    var fader = new myApp.utils.colourFader(base_colour, end_colour);
    fader.setCycle(5000);
    fader.randomStart();

    var v1 = new myApp.vector(500,0);
    var r = 50;
    var p1 = new myApp.point(100,300); 
    var ball1 = new myApp.circle(myCanvas, p1, r);
    ball1.setName('1');
    ball1.setBaseColour(base_colour);
    ball1.setTargetColour(end_colour);
    ball1.setGetColourObject(fader); // We use the fader's getColour method to set the colour
    ball1.setShowShadow(true);
    //ball1.setMass(1);
    var path1 = new myApp.utils.pathLinear(ball1,v1); // Set x and y speed); 
    //var path1 = new myApp.utils.pathParabolic(ball1, v1 ,10); // Set x and y speed); 
    
    ball1.setPath(path1);
    mySimulation.add(ball1);
    
    var v2 = new myApp.vector(400,0);
    var r = 20;
    var p2 = new myApp.point(200,320); 
    var ball2 = new myApp.circle(myCanvas, p2, r);
    ball2.setName('2');
    ball2.setBaseColour(base_colour);
    ball2.setTargetColour(end_colour);
    ball2.setGetColourObject(fader); // We use the fader's getColour method to set the colour
    ball2.setShowShadow(true);
    //ball2.setMass(3);
    var path2 = new myApp.utils.pathLinear(ball2,v2); // Set x and y speed); 
    ball2.setPath(path2);
    mySimulation.add(ball2);
    
 };


var createFadingCircles = function(myCanvas, mySimulation, n) {
    
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    var r;
    for (var i = 0; i<n; i++) {
        // Create a neat circle... in clear space 
        r = seed(40,40);
        c = createRandomCircle(myCanvas, r);
        
        if (c === null)
            return; // Something went wrong.
        
        c.setName(i);
        //c.seed(Math.round(Math.random()*5000)); 
        c.setLifespan(10000); 
        
        var base_colour = new myApp.utils.colour().random();
        var end_colour = new myApp.utils.colour().random();
        var fader = new myApp.utils.colourFader(base_colour, end_colour);
        c.setBaseColour(base_colour);
        c.setTargetColour(end_colour);
        c.setGetColourObject(fader); // We use the fader's getColour method to set the colour
        c.setShowShadow(true);
    
        // Velocity
        var v = new myApp.vector(100+100*Math.random(),100+100*Math.random());
        
        if (Math.random() > 1.5) 
            var path = new myApp.utils.pathParabolic(c, v ,400); // Set x and y speed); 
        else
            var path = new myApp.utils.pathLinear(c, v); // Set x and y speed); 
        c.setPath(path);

        mySimulation.add(c);
    }             
 };

var createColourShiftCircles = function(myCanvas, mySimulation, n) {
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    var r;
    
    for (var i = 0; i<n; i++) {
        // Create a neat circle...
        r = seed(20,20);
        c = createRandomCircle(myCanvas, r);
        if (c !== null) {
            c.seed(Math.round(Math.random()*5000)); 
            c.setLifespan(10000); 
            var base_colour = new myApp.utils.colour().random();
            c.setBaseColour(base_colour);
            c.setFillStyle(myApp.utils.createRadialGradient);
            c.setFadeOut(0);
            c.setShrink(true);
            c.setShowShadow(true);

            // Velocity
            var v = new myApp.vector(-500+1000*Math.random(),-500+1000*Math.random());

            if (Math.random() > 1.5) 
                var path = new myApp.utils.pathParabolic(c, v ,400); // Set x and y speed); 
            else
                var path = new myApp.utils.pathLinear(c, v); // Set x and y speed); 

            c.setPath(path);

            mySimulation.add(c);

        }
    }
 };

var createPermanentBalls = function(myCanvas, mySimulation, n) {
    
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    mySimulation.setTimeStep(30);
    var r;
    
    for (var i = 0; i<n; i++) {
        // Create a neat circle...           
        r = seed(20,30);
        ball = createRandomCircle(myCanvas, r);
        //c_1.seed(Math.round(Math.random()*5000)); 
        //c_1.setLifespan(10000); 
        var base_colour = new myApp.utils.colour().setHex('#000000');
        var end_colour = new myApp.utils.colour().setHex('#FFFFFF');
        var fader = new myApp.utils.colourFader(base_colour, end_colour);
        fader.setCycle(5000);
        fader.randomStart();
        ball.setBaseColour(base_colour);
        ball.setTargetColour(end_colour);
        ball.setGetColourObject(fader); // We use the fader's getColour method to set the colour
        ball.setShowShadow(true);
            // Velocity
            var v = new myApp.vector(100+100*Math.random(),100+100*Math.random());

            if (Math.random() > 1.5) 
                var path = new myApp.utils.pathParabolic(ball, v ,400); // Set x and y speed); 
            else
                var path = new myApp.utils.pathLinear(ball, v); // Set x and y speed); 

        ball.setPath(path);
        mySimulation.add(ball);
    }             
    console.log("Finished here")
 };

 var createWildOrbits = function(myCanvas, mySimulation, n) {

     var last_object;
    var last_radius = 50;

    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    var dummy_p = new myApp.point(0,0);
    
    // Create the centre of the world
    var p = new myApp.point(myCanvas.width/2,myCanvas.height/2);
    var c_1 = new myApp.circle(myCanvas, p, last_radius);
    c_1.setBaseColour(randomColour());
    c_1.setFillStyle(myApp.utils.createRadialGradient);
    var path = new myApp.utils.pathStationary(c_1);  
    c_1.setPath(path);
    c_1.setDensity(0);     // No collisions
    c_1.setBounded(false);
    mySimulation.add(c_1);                 
    last_object = c_1;

     for (var i = 0; i<n; i++) {
        var c_1 = new myApp.circle(myCanvas, dummy_p, Math.round(10+Math.random()*10));
        c_1.setBaseColour(randomColour());
        c_1.setFillStyle(myApp.utils.createRadialGradient);
        c_1.setFadeOut(0);
        c_1.setShrink(true);
        c_1.setShowShadow(true);
        c_1.setDensity(0);     // No collisions
        c_1.setBounded(false);
        o_centre = last_object;
        var path = new myApp.utils.pathOrbit(c_1, last_object, seed(50,200), randomAngle(), (Math.random()*8-2)*Math.PI/4);  
        path.setBias(seed(.8,2));
        c_1.setPath(path);                

        mySimulation.add(c_1);
        // Add satellites
    }             
 };

 var solarSystem = function(myCanvas, mySimulation, planets, moons) {

    var last_object;
    var last_radius = 30;
    var last_planet_orbit = 50;
    var planet_radius;
    var last_planet_year = seed(1,3)*Math.PI/16;
    var moon_orbit;

    mySimulation.setTimeStep(20);
    
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    
    // Create the centre of the world
    var p = new myApp.point(myCanvas.width/2,myCanvas.height/2)
    var sun = new myApp.circle(myCanvas, p, last_radius);
    sun.setBaseColour(new myApp.utils.colour().random());
    sun.setFillStyle(myApp.utils.createRadialGradient);
    sun.setDensity(0);
    var path = new myApp.utils.pathStationary(sun);  
    sun.setPath(path);                
    mySimulation.add(sun);                            

    // Planets!!
    for (var i = 0; i<planets; i++) {
        planet_orbit = last_planet_orbit + seed(50,50);
        planet_year = last_planet_year * .75;
        planet_radius = seed(10,10);

        p = new myApp.point(0,0);
        var planet = new myApp.circle(myCanvas, p, planet_radius);
        planet.setBaseColour(new myApp.utils.colour().random());
        planet.setFillStyle(myApp.utils.createRadialGradient);
        planet.setFadeOut(0);
        planet.setShrink(true);
        planet.setShowShadow(true);
        planet.setDensity(0);
        var path = new myApp.utils.pathOrbit(planet, sun, planet_orbit, randomAngle(), planet_year);  
        planet.setPath(path);                

        mySimulation.add(planet);
        moon_orbit = planet_radius + 10;
        // Add satellites
        for (var j = 0; j<Math.random()*moons; j++) {
            p = new myApp.point(0,0);
            var moon = new myApp.circle(myCanvas, p, seed(2,4));
            moon.setBaseColour(new myApp.utils.colour().random());
            moon.setFillStyle(myApp.utils.createRadialGradient);
            moon.setFadeOut(0);
            moon.setShrink(true);
            moon.setShowShadow(true);
            moon.setDensity(0);
            var path = new myApp.utils.pathOrbit(moon, planet, moon_orbit, randomAngle(), planet_year*(seed(1,8)));  
            moon.setPath(path);                

            mySimulation.add(moon);    
            moon_orbit += 8;
        }
        last_planet_orbit = planet_orbit;
        last_planet_year  = planet_year;
}    

};

var createFallingStars = function(myCanvas, mySimulation, n) {
    
    // Give the canvas a radial gradient with a focussed shadow
    dark = new myApp.utils.colour().setRGB(0,0,61);
    dusk = new myApp.utils.colour().setHex('#660000');
    myCanvas.setBaseColour();
    myCanvas.setFillStyle(myApp.utils.createLinearGradient(myCanvas, myCanvas, dark, dusk ));
    //mySimulation.setTimeStep(10);
    //myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    
    for (var i = 0; i<n; i++) {
        mySimulation.add(createFallingStar());
    }             
 };

var createFallingStar = function() {
    
    var p = new myApp.point(myCanvas.width/2*3*Math.random(),0);
    var fs = new myApp.circle(myCanvas, p, seed(1,1), "fs");
    fs.seed(Math.round(Math.random()*20000)); 
    fs.setBounded(false); 

    //var base_colour = new myApp.utils.colour().setHex('#E6E6E6');
    var base_colour = new myApp.utils.colour().setHex('#FFFFFF');
    fs.setBaseColour(base_colour);
    //c_1.setFillStyle(c_1.fillStyleColour);
    fs.setShowShadow(true);
    fs.setFadeOut(0);
    fs.setShowShadow(true);

    // Set the path - 50% parabolic, 50% straight
    var x_speed = seed(300,300);
    var y_speed = seed(20,30);
    var v = new myApp.vector(x_speed, y_speed)
    if (Math.random() > 1.5) 
        var path = new myApp.utils.pathParabolic(fs,x_speed, y_speed,400); // Set x and y speed); 
    else
        var path = new myApp.utils.pathLinear(fs, v); // Set x and y speed); 
    fs.setPath(path);

    var lifespan = Math.round((.5 * (myCanvas.height / x_speed))*1000);
    fs.setLifespan(lifespan);
    
    return (fs);
};


var moon = function(myCanvas, mySimulation, n) {

    mySimulation.setTimeStep(80);
    
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setBaseColour(new myApp.utils.colour().setHex('#C2EBEF'));
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    mySimulation.setTimeStep(100);
    
    var G = 1;
    
    var satellite1 = createGravitationalBall(myCanvas, "Planet", G, myCanvas.width/2, 250, 10, 0, 0);
    satellite1.setDensity(1);
    mySimulation.add(satellite1);
       
    var satellite2 = createGravitationalBall(myCanvas, "Moon", G, myCanvas.width/2, 218, 5, 10, 2);
    satellite2.setDensity(.0001);
    mySimulation.add(satellite2);
 };

var capturedOrbit = function(myCanvas, mySimulation, n) {
    
    var G = 1;

    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setBaseColour(new myApp.utils.colour().setHex('#C2EBEF'));
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    
    // Create the centre of the world
    var p = new myApp.point(myCanvas.width/2,myCanvas.height/2);
    var sun = new myApp.circle(myCanvas, p, 30);
    sun.setBaseColour(new myApp.utils.colour().random());
    sun.setFillStyle(myApp.utils.createRadialGradient);
    var path = new myApp.utils.pathStationary(sun);  
    sun.setPath(path);            
    sun.setDensity(50);
    mySimulation.add(sun);                            
    mySimulation.setTimeStep(40);

    var satellite1 = createGravitationalBall(myCanvas, "sat1", G, myCanvas.width, 50, 5, -50, -25);
    satellite1.setDensity(.02);
    console.log(satellite1);
    mySimulation.add(satellite1);

    var satellite2 = createGravitationalBall(myCanvas, "sat2", G, 0, 150, 5, 40, -35);
    satellite2.setDensity(.01);
    satellite2.seed(2000);
    mySimulation.add(satellite2);
    //mySimulation.add(createGravitationalBall(myCanvas, 0, 400, 10, 80, 0));    
       
 };
 
var capturedOrbitScaled = function(myCanvas, mySimulation, n) {
    
    var G = 1;

    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setBaseColour(new myApp.utils.colour().setHex('#C2EBEF'));
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    myCanvas.setScalingFactor(10);
    
    // Create the centre of the world
    var p = new myApp.point(myCanvas.width/2,myCanvas.height/2);
    var sun = new myApp.circle(myCanvas, p, 3);
    sun.setBaseColour(new myApp.utils.colour().random());
    sun.setFillStyle(myApp.utils.createRadialGradient);
    var path = new myApp.utils.pathStationary(sun);  
    sun.setPath(path);            
    sun.setDensity(6000);
    mySimulation.add(sun);                            
    mySimulation.setTimeStep(100);

    var satellite1 = createGravitationalBall(myCanvas, "sat1", G, myCanvas.width/2, 50, .5, -40, -10);
    satellite1.setDensity(.2);
    console.log(satellite1);
    mySimulation.add(satellite1);

    var satellite2 = createGravitationalBall(myCanvas, "sat2", G, 0, 100, .5, 30, -10);
    satellite2.setDensity(.1);
    satellite2.seed(2000);
    mySimulation.add(satellite2);
    //mySimulation.add(createGravitationalBall(myCanvas, 0, 400, 10, 80, 0));    

    var satellite3 = createGravitationalBall(myCanvas, "sat1", G, myCanvas.width/2, myCanvas.height-50, .5, 40, 10);
    satellite3.setDensity(.2);
    console.log(satellite3);
    mySimulation.add(satellite3);

       
 };
 
 var ions = function(myCanvas, mySimulation, n) {
    
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setBaseColour(new myApp.utils.colour().setHex('#C2EBEF'));
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    mySimulation.setTimeStep(20);

    
    for (var i = 0; i<n; i++) {
        var ion = createIon(myCanvas, "Ion."+i,
                                      seed(100,myCanvas.width-200),
                                      seed(100,myCanvas.width-200),
                                      10, 
                                      seed(200,100),
                                      seed(200,100));
        ion.setDensity(.1);
        ion.setCharge(100);
        if (Math.random() > 1.2)
            ion.setVisible(false);
        mySimulation.add(ion);
        
    }       

 };

var dust = function(myCanvas, mySimulation, n,m) {
    
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    mySimulation.setTimeStep(30);
    var r;
    var base_colour = new myApp.utils.colour().setRGB(0,25,61);
    
    // Create the dust particles
    for (var i = 0; i<n; i++) {
        // Create a neat circle...           
        ball = createRandomCircle(myCanvas, 5);
        ball.setBaseColour(base_colour);
        var v = new myApp.vector(0,0)
        var path = new myApp.utils.pathLinear(ball,v); // Set x and y speed); 
        ball.setPath(path);
        ball.setBounded(true);
        ball.setAnnotate(false);
        mySimulation.add(ball);
    }
    console.log("Created dust ",n);

    // Create the unseen gas particles
    for (var i = 0; i<m; i++) {
        // Create a neat circle...           
        ball = createRandomCircle(myCanvas, 2);
        ball.setBaseColour(base_colour);
        var v = new myApp.vector(0,0)
        var path = new myApp.utils.pathLinear(ball,v); // Set x and y speed); 
        ball.setPath(path);
        ball.setBounded(true);
        ball.setVisible(false);
        var v = new myApp.vector(100+100*Math.random(),100+100*Math.random());
        var path = new myApp.utils.pathLinear(ball, v); // Set x and y speed); 
        ball.setPath(path);
        mySimulation.add(ball);
    }
    console.log("Created gas ",n);
 };

var createGravitationalBall = function(myCanvas, name, G, x, y, r, x_speed, y_speed) {
    
    console.log(x,y,r,x_speed,y_speed);
    // Create a neat circle...    
    var p = new myApp.point(x,y);
    var ball = new myApp.circle(myCanvas, p, r, name);

    var base_colour = new myApp.utils.colour().setHex('#006699');
    var end_colour = new myApp.utils.colour().setHex('#3333FF');
    var fader = new myApp.utils.colourFader(base_colour, end_colour);
    fader.setCycle(5000);
    fader.randomStart();
    ball.setBaseColour(base_colour);
    ball.setTargetColour(end_colour);
    ball.setGetColourObject(fader); // We use the fader's getColour method to set the colour
    ball.setShowShadow(true);
    var v = new myApp.vector(x_speed, y_speed)
    var path = new myApp.utils.pathGravitational(ball, v); // Set x and y speed); 
    path.setG(G);
    ball.setPath(path);
    ball.setBounded(false);
    //ball.setAnnotate(true);
    console.log("Created gb:",ball.toString())

    return(ball);

};

var createRandomCircle = function(myCanvas, r) {
    var j = 0;
    do {
        var p = new myApp.point(seed(40,myCanvas.width-80),seed(40,myCanvas.height-80));
        console.log("CreateRandomCircle Trying ",p);
        var c = new myApp.circle(myCanvas, p, r);
        console.log("CreateRandomCircle Tried ",p);
        if (j++ > 20) {
            console.log("Looped to many times creating circle?? ",p);
            return null;
        }
    } while (myCanvas.checkCollisions(c, p).length > 0);
    
    return c;
}


var createIon = function(myCanvas, name, x, y, r, x_speed, y_speed) {
    
    // Create a neat circle...    
    var p = new myApp.point(x,y);
    var ion = new myApp.circle(myCanvas, p, r, name);

    var base_colour = new myApp.utils.colour().setRGB(0,25,61);
    ion.setBaseColour(base_colour);
    ion.setBaseColour(base_colour);
    ion.setShowShadow(false);
    var v = new myApp.vector(x_speed, y_speed);
    var path = new myApp.utils.pathIon(ion,v); // Set x and y speed); 
    console.log("Ion.path.v",v.x, v.y);
    ion.setPath(path);
    ion.setBounded(true);
    ion.setAnnotate(false);
    return(ion);

};


var randomColour = function() {
    var new_colour = new myApp.utils.colour(Math.round(Math.random()*255), 
                                            Math.round(Math.random()*255),
                                            Math.round(Math.random()*255));
    return (new_colour);
};

var createRadialGradient = function(myCanvas, r1, r2, c1, c2) {
    // create radial gradient
    // Randomly create gradient focus for first cut 
    var gx = Math.round(Math.random(myCanvas.width/2)*myCanvas.width/2+myCanvas.width/4);
    var gy = Math.round(Math.random(myCanvas.height/2)*myCanvas.height/2+myCanvas.height/4);

    var grd = myCanvas.ctx.createRadialGradient(gx, gy, r1, gx, gy, r2);
    // light blue grd.addColorStop(0, '#C2EBEF');
    grd.addColorStop(0, c1);
    // dark blue grd.addColorStop(1, '#478FB2');            
    grd.addColorStop(1, c2);            

    response = [];
    response.focus = [];
    response.focus.x = gx;
    response.focus.y = gy;
    response.grd = grd;

    return(response);                
};

var seed = function(min, range) {
    return (Math.round(min+Math.random()*range));
};

var randomAngle = function() {
    return (Math.random()*2*Math.PI);
};

var createGravitationalBallsFixed = function(myCanvas, mySimulation) {
    
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    myCanvas.setScalingFactor(1000);
    mySimulation.setTimeStep(100);
    
    var G = 70000;
    
    var ball1 = createGravitationalBall(myCanvas, "Ball 1",
                                                  G,
                                                  100,
                                                  101,
                                                  .01, 
                                                  100,
                                                  0);
    ball1.setDensity(10000);
    ball1.setBounded(true);
    mySimulation.add(ball1);
    console.log(ball1);
            
    var ball2 = createGravitationalBall(myCanvas, "Ball 2",
                                                  G,
                                                  600,
                                                  100,
                                                  .01, 
                                                  -100,
                                                  0);
    ball2.setDensity(10000);
    ball2.setBounded(true);
    console.log(ball2);
    mySimulation.add(ball2);
            
 };

var createGravitationalBallsRandom = function(myCanvas, mySimulation, n) {
    
    // Give the canvas a radial gradient with a focussed shadow
    resp = createRadialGradient(myCanvas, 10, 300, '#C2EBEF','#478FB2' );
    myCanvas.setFillStyle(resp.grd);
    myCanvas.setShadowFocus(resp.focus.x, resp.focus.y);
    myCanvas.setScalingFactor(1000);
    myCanvas.setEscapeVelocity(1000);
    mySimulation.setTimeStep(100);
    
    var G = 70000;
    var y = 200;
    
    for (var i=0; i<n; i++) {
        if (i%2 === 1) {
            speed = -20;
            x = myCanvas.width/2+i*40;
        }
        else {
            speed = 20;
            x = myCanvas.width/2-i*40;
        }
            
        var ball = createGravitationalBall(myCanvas, "Ball "+i,
                                                     G,
                                                     x,
                                                     y+i*20,
                                                    .01, 
                                                     speed,
                                                     0);
        ball.setDensity(10000);
        ball.setBounded(true);
        ball.setAnnotate(true);
        console.log(ball);
        mySimulation.add(ball);
        
    }       
 };

