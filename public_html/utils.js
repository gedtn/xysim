function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function showNumber(n, p) {
    if (n > 10000)
        return n.toExponential(p);
    else
        return n.toFixed(p);
}

function calcFinalMomentum(m1, p1, m2, p2) {    
    return (2*m2*p1+(m2-m1)*p2)/(m1+m2);
}

function calcCollisionVelocity(o1, o2) {
    
    var m1 = o1.getMass();
    var v1 = o1.path.velocity.scalar;
    var a1 = o1.path.velocity.getAngle();

    var m2 = o2.getMass();
    var v2 = o2.path.velocity.scalar;
    var a2 = o2.path.velocity.getAngle();
    var a =  o1.centre.getAngle(o2.centre);

    var v_x = (v1 * Math.cos(a1 - a)*(m1-m2)+2*m2*v2*Math.cos(a2-a))/(m1+m2)*Math.cos(a) + 
              v1*Math.sin(a1-a)*Math.cos(a+Math.PI/2);
    var v_y = ((v1 * Math.cos(a1 - a)*(m1-m2)+2*m2*v2*Math.cos(a2-a))/(m1+m2))*Math.sin(a) +
              (v1*Math.sin(a1-a)*Math.sin(a+Math.PI/2));

    var final_v = new myApp.vector(v_x, v_y);

return final_v;
}

myApp.utils = (function() {
    
    var G = 1; //psuedo gravitational constant
    var k = 120000; //psuedo Coulomb's constant constant
    
    var drawVector = function(vector, obj) {
        
        document.getElementById("vector").innerHTML = "dV? ";
        var angle = vectorAngle(vector.x, vector.y);        
        var x;
        var y;
        var r = obj.r*obj.canvas.scalingFactor;
        
        // Now find starting point on circumference
        var start_x = obj.x + Math.cos(angle) * r;        
        var start_y = obj.y + Math.sin(angle) * r;
        var element = document.getElementById("vector");

        if (element !== null) 
            document.getElementById("vector").innerHTML = 
                    "x:"+obj.x.toFixed(3)+
                    ",y:"+obj.y.toFixed(3)+
                    ",r:"+obj.r.toFixed(3)+","+r.toFixed(3)+
                    ",angle:"+angle.toFixed(3)+
                    ",start_x:"+start_x.toFixed(3)+
                    ",start_y:"+start_y.toFixed(3);
        
        // Now draw it - head first, then body
        ctx = obj.canvas.ctx;
        ctx.strokeStyle = obj.canvas.baseColour.getContrast().toString();
        ctx.beginPath();
        x = 10 * Math.cos(angle+Math.PI/6);
        y = 10 * Math.sin(angle+Math.PI/6);
        ctx.moveTo(start_x + x,start_y + y);
        ctx.lineTo(start_x, start_y);
        x = 10 * Math.cos(angle-Math.PI/6);
        y = 10 * Math.sin(angle-Math.PI/6);
        ctx.lineTo(start_x + x,start_y + y);
        ctx.stroke();
        
        ctx.beginPath();
        x = 40 * Math.cos(angle);
        y = 40 * Math.sin(angle);
        ctx.moveTo(start_x + x,start_y + y);
        ctx.lineTo(start_x, start_y);
        ctx.stroke();

        var font_size = 20;
        ctx.fillStyle = obj.canvas.baseColour.getContrast().toString();
        ctx.font= font_size+"px Georgia";
        var disp_angle = angle/(2*Math.PI)*360
        ctx.fillText("Force:"+vector.scalar.toExponential(),start_x + x,start_y + y);
    };
    
    var sumVectors = function(vectors) {

        var den;
        var x_sum = 0;
        var y_sum = 0;
        var s_sum = 0;
        
        // Loop through vectors, determine force over x and y coords, and sum
        for (var i = 0; i<vectors.length; i++) {
            v = vectors[i];

            den = Math.sqrt(Math.pow(v.x,2)+Math.pow(v.y,2));
            x_sum += v.scalar * v.x/den;
            y_sum += v.scalar * v.y/den;
            s_sum += v.scalar;
        }
        
        return(new myApp.vector(x_sum, y_sum));
    };
    
    var sumVectors_old = function(vectors) {

        // I think this is right!!
        var den;
        var x_sum = 0;
        var y_sum = 0;
        var s_sum = 0;
        var vector_sum = [];
        
        // Loop through vectors, determine force over x and y coords, and sum
        for (var i = 0; i<vectors.length; i++) {
            v = vectors[i];
            den = Math.sqrt(Math.pow(v.x,2)+Math.pow(v.y,2));
            x_sum += v.scalar * v.x/den;
            y_sum += v.scalar * v.y/den;
            s_sum += v.scalar;
        }
        
        vector_sum.x = x_sum;
        vector_sum.y = y_sum;
        vector_sum.scalar = s_sum;
        return(vector_sum);
    };
    
    var vectorScalar = function (x,y) {
        return (Math.sqrt(Math.pow(x,2)+Math.pow(y,2)));    
    };
    
    var vectorAngle = function (x,y) {
        // Watch out for zero divide
        if (x === 0)
            angle = Math.PI/2;
        else
            //angle = Math.atan(vector.y_force / vector.x_force);
            angle = Math.atan(y / x);

        // Adjust quadrants to correct angle
        if (x < 0)
            angle += Math.PI;
        
        return(angle);
    };
    
    var pointsDistance = function (x1,y1, x2, y2) {
        return (Math.sqrt(Math.pow(x1-x2,2)+Math.pow(y1-y2,2)));    
    };
    
    // I think this is untested
    var createRadialGradient = function (canvas, obj) {
            
        x = obj.centre.x;
        y = obj.centre.y;
        r = obj.r;

        // No point worrying about gradient for small objects
        if (r > 5) {            
            var grd=canvas.ctx.createRadialGradient(x,y,5,x,y,r);
            grd.addColorStop(0,"white");
            grd.addColorStop(1,obj.getBaseColour().toString());
            return (grd);
        }
        else {
            return (this.baseColour.toString());
        }
    };

    // I think this is untested
    var createLinearGradient = function (canvas, obj, col1, col2) {
            
        c1 = obj.extents.low;   // top left corner
        c2 = obj.extents.high;   // bottom right corner
        size = Math.sqrt(Math.pow(c2.x-c1.x,2)+Math.pow(c2.y-c1.y,2));

        // No point worrying about gradient for small objects
        if (size > 5) {            
            //var grd=canvas.ctx.createLinearGradient(c1.x,c1.y,c2.x,c2.y);
            var grd=canvas.ctx.createLinearGradient(0,0,0,c2.y);
            grd.addColorStop(0,col1.toString());
            grd.addColorStop(1,col2.toString());
            return (grd);
        }
        else {
            return (this.baseColour.toString());
        }
    };

    var colour = function() {
        this.r = 0;
        this.g = 0;
        this.b = 0;
    };

    colour.prototype = {
        setRGB:function(r, g, b) {
            this.r = r;
            this.g = g;
            this.b = b;
            return (this);
        },
        random:function() {
            this.r = Math.round(Math.random()*255); 
            this.g = Math.round(Math.random()*255); 
            this.b = Math.round(Math.random()*255); 
            return (this);
        },
        setHex:function (hex) {
            var resp = this.decodeHex(hex);
            if (resp === null)
                return null;
            else {
                this.r = resp.r;
                this.g = resp.g;
                this.b = resp.b;
                return (this);
            }
        },
        // With thanks to Tim Down at Stack Overflow
        decodeHex:function(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        },
        // Return a colour that will contrast this colour, thanks to 24ways.com
        getContrast:function() {
            
            yiq = ((this.r*299)+
                   (this.g*587)+
                   (this.b*114))/1000;
           
            return (yiq >= 128) 
                    ? new myApp.utils.colour().setRGB(0,0,0) 
                    : new myApp.utils.colour().setRGB(255,255,255);
        },        
        // Default is 'rgb(r,g,b)'
        toString:function() {
            return('rgb('+this.r+','+this.g+','+this.b+')');
        },
        toStringHex:function() {
            return "#" + componentToHex(this.r) +
                         componentToHex(this.g) +
                         componentToHex(this.b);
            
        }
    };

    var colourFader = function (colour_start, colour_end) {    
        this.colour_start = colour_start;
        this.colour_end = colour_end;
        this.cycle = -1;
        this.offset = 0; // Allows for random starts
        //console.log(this);
    };
    
    colourFader.prototype = {
        setCycle:function (time) {
            this.cycle = time;
        },
        randomStart:function (){
            this.offset = Math.round(Math.random()*this.cycle);
        },
        getColour:function (obj) {
            
            var stage; 
            
            // if cycle is set then we will use that to fade colour in/out
            if (this.cycle !== -1) 
                stage = ((obj.runTime+this.offset)%this.cycle)/this.cycle;
            else
                // Otherwise it is designed to reflect lifestage of object
                stage = obj.getLifeRemaining();
            
            //console.log(this.cycle,stage);
            var new_colour = new colour();
            var new_r = this.calcHue(this.colour_start.r,this.colour_end.r, stage);
            var new_g = this.calcHue(this.colour_start.g,this.colour_end.g, stage);
            var new_b = this.calcHue(this.colour_start.b,this.colour_end.b, stage);
            new_colour.setRGB(new_r, new_g, new_b);
            return (new_colour);
        },
        calcHue: function(s, e, stage) {
            return (Math.round((e-s)*stage+s));
        }
        
    };
    
    var checkBoundaryBounce = function (pathObj, next_x, next_y) {

        var next_pos = [];
        var response = [];
        var boundary_collision = null;

        if (pathObj.obj.bounded) {

            c_width = pathObj.obj.canvas.width;
            c_height = pathObj.obj.canvas.height;

            // Have we hit far side?
            boundary_collision = pathObj.obj.checkBoundary(next_x, next_y);
            if (boundary_collision.right) {
                pathObj.xSpeed = 0 - pathObj.xSpeed;
                next_x -= boundary_collision.x_rebound*2;
            }
            // Or, have we hit near side?
            else if (boundary_collision.left) {
                pathObj.xSpeed = 0 - pathObj.xSpeed;
                next_x += boundary_collision.x_rebound*2;
            }

            // Have we hit the bottom      
            if (boundary_collision.bottom) {
                pathObj.ySpeed = 0 - pathObj.ySpeed;
                next_y -= boundary_collision.y_rebound*2;
            }
            // Have we hit the top?
            else if (boundary_collision.top) {
                pathObj.ySpeed = 0 - pathObj.ySpeed;
                next_y += boundary_collision.y_rebound*2;    
            }        

            next_pos.x = next_x;
            next_pos.y = next_y;
        }
        else { // Unbounded object
            boundary_collision = {left: false, right: false, top:false, bottom:false}; //1 = collision
            
            next_pos.x = next_x;
            next_pos.y = next_y;
        }

        response.next_pos = next_pos;
        response.collisions = boundary_collision;

        return (response);            
    };

    var pathStationary = function (obj) {
        this.obj    = obj;
        this.velocity = new myApp.vector(0,0);
    };

    pathStationary.prototype = {
        toString:function() {
            return ("is stationary");
        },        
        getVelocity: function() {
            return 0;
        },
        getNextPos:function (i_time) {

            return (this.obj.centre);
        }
    };
  
    var pathLinear = function (obj, velocity) {
        this.obj    = obj;
        this.velocity = velocity;
    };

    pathLinear.prototype = {
        toString:function() {
            return ("Linear trajectory at "+this.velocity.toString());
        },
        getVelocity: function() {
            return this.velocity.scalar;
        },
        getNextPos:function (i_time) {

            var x_inc = this.velocity.x * i_time / 1000;
            var y_inc = this.velocity.y * i_time / 1000;

            var next_pos = new myApp.point(this.obj.centre.x + x_inc,this.obj.centre.y + y_inc);

            return (next_pos);
        }
    };
  
    var pathParabolic = function (obj, velocity, force) {
        this.obj    = obj;
        this.velocity = velocity;
        this.velocity.setAfterChangeCallback(this,this.changeVelocity);
        console.log("Callback? ",this);
        this.iVelocity = velocity; // initial vertical speed
        this.force  = force;
        this.trajectoryTime = 0;
    };
    
    pathParabolic.prototype = {
        toString:function() {
            return (Math.round(this.xSpeed)+"+"+Math.round(this.ySpeed));
        },
        getVelocity: function() {
            return this.velocity.scalar;
        },
        changeVelocity:function(path, old_v, new_v) {
            console.log("In callback ",path, new_v.y);
            path.iVelocity = new_v;
            path.trajectoryTime = 0;
        },
        getNextPos:function (i_time) {

            var next_pos = new myApp.point();
            
            this.trajectoryTime = this.trajectoryTime + i_time;
            
            // Calc new vertical speed
            this.velocity.y = this.iVelocity.y + this.force * this.trajectoryTime/1000;
            
            // Apply friction, assumed constant and applies to both vectors
            this.velocity.y = this.velocity.y - this.velocity.y * this.obj.frictionAir * i_time/1000;
            this.velocity.x = this.velocity.x - this.velocity.x * this.obj.frictionAir * i_time/1000;

            // OK, now calc next position
            next_pos.x = this.obj.centre.x + i_time/1000 * this.velocity.x;        
            next_pos.y = this.obj.centre.y + i_time/1000 * this.velocity.y;

            return (next_pos);
        }
    };
  
    var pathOrbit = function (obj, centre_obj, r, angle, angular_speed) {
        this.obj    = obj;
        this.centre_obj = centre_obj;
        this.r      = r;
        this.angle  = angle; // Starting angle in Radians
        this.angular_speed  = angular_speed;  // radians per second
        this.bias = obj.canvas.width/obj.canvas.height;
        // scalar velocity = circumference * angular speed / 2pi
        this.velocity = new myApp.vector().setScalar((2 * Math.PI * this.r) * this.angular_speed / (Math.PI * 2)) 
    };

    pathOrbit.prototype = {
        toString:function() {
            return (this.xSpeed+"+"+this.ySpeed);
        },
        getVelocity: function() {
            return this.velocity.scalar;
        },
        setBias: function(b) {
            this.bias = b;
        },
        getNextPos:function (i_time) {

            var next_pos = new myApp.point();

            // We take the run time for the object (in seconds), 
            // multiply by angular velocity and add starting angle
            var new_angle = this.angle + this.angular_speed * this.obj.runTime/1000;

            next_pos.x = this.centre_obj.centre.x + this.bias*Math.cos(new_angle)*this.r;
            next_pos.y = this.centre_obj.centre.y + Math.sin(new_angle)*this.r;

            return (next_pos);
            //return (myApp.utils.checkBoundaryBounce(this, next_x, next_y).next_pos);            
        }
    };
  
    var pathGravitational = function (obj, v) {
        this.obj    = obj;
        this.velocity = v;
        this.G = myApp.utils.G;
    };

    pathGravitational.prototype = {
        toString:function() {
            return (Math.round(this.xSpeed)+"+"+Math.round(this.ySpeed));
        },
        setG:function(G) {
            this.G = G;
        },
        getVelocity: function() {
            return this.velocity.scalar;
        },
        getNextPos:function (i_time) {

            var next_pos = new myApp.point();
            
            // First we calculate gravitational pull and apply it to speed
            force_vector = this.obj.calcGravity(this.G);
            
            console.log("Path grav.x: ",this.velocity.x, force_vector.x,this.velocity.x - force_vector.x/this.obj.getMass() * i_time/1000)
            console.log("Path grav.y: ",this.velocity.y, force_vector.y,this.velocity.y - force_vector.y/this.obj.getMass() * i_time/1000)

            // Calc new speeds
            this.velocity.x = this.velocity.x + force_vector.x/this.obj.getMass() * i_time/1000;
            this.velocity.y = this.velocity.y + force_vector.y/this.obj.getMass() * i_time/1000;

            var x_inc = this.velocity.x * i_time / 1000;
            var y_inc = this.velocity.y * i_time / 1000;

            next_pos.x = this.obj.centre.x + x_inc;
            next_pos.y = this.obj.centre.y + y_inc;

            return (next_pos);
        }
    };
  
    var pathIon = function (obj, velocity) {
        this.obj    = obj;
        this.velocity = velocity;
    };

    pathIon.prototype = {
        toString:function() {
            return (Math.round(this.xSpeed)+"+"+Math.round(this.ySpeed));
        },
        getVelocity: function() {
            return this.velocity.scalar;
        },
        getNextPos:function (i_time) {
            
            var next_pos = new myApp.point();
            
            // First we calculate gravitational pull and apply it to speed
            force_vector = this.obj.calcElectroForce();

            // Calc new speeds
            this.velocity.x = this.velocity.x - force_vector.x/this.obj.getMass() * i_time/1000;
            this.velocity.y = this.velocity.y - force_vector.y/this.obj.getMass() * i_time/1000;
//console.log("pathIon.fv2: ",this.xSpeed);

            var x_inc = this.velocity.x * i_time / 1000;
            var y_inc = this.velocity.y * i_time / 1000;

            next_pos.x = this.obj.centre.x + x_inc;
            next_pos.y = this.obj.centre.y + y_inc;

            return (next_pos);
        }
    };
    
    
    return {G: G,
            k: k,
            sumVectors: sumVectors,
            sumVectors_old: sumVectors_old,
            pointsDistance: pointsDistance, 
            createRadialGradient: createRadialGradient,
            createLinearGradient: createLinearGradient, 
            colour: colour,
            colourFader: colourFader,
            pathStationary: pathStationary,
            pathOrbit: pathOrbit,
            pathLinear: pathLinear,
            pathGravitational: pathGravitational,
            pathParabolic: pathParabolic,
            pathIon: pathIon,
            checkBoundaryBounce: checkBoundaryBounce    
            };
})();
