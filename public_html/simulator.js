

window.requestAnimFrame = (function(callback) {
    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
    function(callback) {
      window.setTimeout(callback, 1000 / 60);
    };
})();    
    
myApp = (function() {
    
    var STATE_SEED    = 1;
    var STATE_ACTIVE  = 2;
    var STATE_STOPPED = 3;
    var STATE_EXPIRED = 4;
    
    // Idea here is to encapsulate common functions and have different object types inherit this....    
    var inheritsFrom = function (child, parent) {
        child.prototype = Object.create(parent.prototype);
    };    

    var simulationObject = function() {
        this.id = 0;
        this.stopped = false;
        this.lifespan = -1;
        this.runTime = 0;
        this.testMsg = "Is this inherited?";
    };
    
    simulationObject.prototype = {
        set:function(name,value) {
            this.context[name] = value;
        },
        get:function(name) {
            return (this.context[name]);
        }
    };
    
    var point = function(x,y) {
        this.x = x;
        this.y = y;
    };
    
    point.prototype = {
        distance: function (point) {
            return (Math.sqrt(Math.pow(this.x - point.x,2)+Math.pow(this.y-point.y,2)));    
        },
        getAngle: function (point) {
            var v = new myApp.vector(point.x - this.x,point.y - this.y)
            return (v.getAngle());
        },
        toString: function() {
        	return ("("+this.x.toFixed(2)+","+this.y.toFixed(2)+")");            
        }
    };

    var vector = function (x,y) {
        // Can be instantiated without initial x,y
        if (typeof x === 'undefined') 
            this.x = null;
        else 
            this.x = x;
        if (typeof y === 'undefined') 
            this.y = null;
        else 
            this.y = y;
        
        this.calcScalar();
        this.afterChangeCallback = null; // Ability to perform actions when change
    };
    
    vector.prototype = {
        toString: function() {
            return (showNumber(this.scalar,3) + " (" +
                    showNumber(this.x,3) + "," +
                    showNumber(this.y,3)+")");
        },
        setX: function (x) {
            this.x = x;
            this.calcScalar();
        },
        setY: function (y) {
            this.y = y;
            this.calcScalar();
        },
        calcScalar:function() {
            if ((this.x !== null) && (this.y !== null))
                this.scalar = Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2)); 
            else
                this.scalar = null;            
        },
        setAfterChangeCallback: function(obj, method) {
            // Ability to perform actions when change
            this.afterChangeCallback = [];
            this.afterChangeCallback.obj = obj; 
            this.afterChangeCallback.method = method;
            console.log("setting cb ",this.afterChangeCallback);
        },
        setScalar: function(scalar, angle) {
            
            var this_old = new myApp.vector(this.x, this.y);
            this.scalar = scalar;

            // We can invoke without angle, and use vector's existing angle
            if (typeof angle !== "number")
                angle = this.getAngle();
            
            this.x = Math.cos(angle) * scalar;
            this.y = Math.sin(angle) * scalar;
            
            // Do we have a change callback?
            if (this.afterChangeCallback !== null)
                this.afterChangeCallback.method(this.afterChangeCallback.obj, this_old, this);
            
            return(this);
        },
        subtract: function(v) {
            var this_old = new myApp.vector(this.x, this.y);

            this.x -= v.x;
            this.y -= v.y;
            this.calcScalar(); 

            // Do we have a change callback?
            if (this.afterChangeCallback !== null)
                this.afterChangeCallback.method(this.afterChangeCallback.obj, this_old, this);
        },
        add: function(v) {
            var this_old = new myApp.vector(this.x, this.y);

            this.x += v.x;
            this.y += v.y;
            this.calcScalar(); 

            // Do we have a change callback?
            if (this.afterChangeCallback !== null)
                this.afterChangeCallback.method(this.afterChangeCallback.obj, this_old, this);
        },
        getAngle: function () {
            // Watch out for zero divide
            if (this.x === 0)
                var angle = Math.PI/2;
            else
                var angle = Math.atan(this.y / this.x);

            // Adjust quadrants to correct angle
            if (this.x < 0)
                angle += Math.PI;

            return(angle);
        },
        reflect: function (r_angle) {
            var this_old = new myApp.vector(this.x, this.y);
            // I think the following correctly calculates the angle of relection
            var new_angle = 2 * r_angle - this.getAngle();

            // Now use new angle to recalc the vector
            this.setScalar(this.scalar, new_angle);

            // Do we have a change callback?
            if (this.afterChangeCallback !== null)
                this.afterChangeCallback.method(this.afterChangeCallback.obj, this_old, this);
        },
        draw: function(canvas, head) {

            var x;
            var y;

            // Now draw it - head first, then body
            ctx = canvas.ctx;
            ctx.strokeStyle = canvas.baseColour.getContrast().toString();
            ctx.beginPath();
            x = 10 * Math.cos(this.getAngle()+Math.PI/6);
            y = 10 * Math.sin(this.getAngle()+Math.PI/6);
            ctx.moveTo(head.x + x,head.y + y);
            ctx.lineTo(head.x, head.y);
            x = 10 * Math.cos(this.getAngle()-Math.PI/6);
            y = 10 * Math.sin(this.getAngle()-Math.PI/6);
            ctx.lineTo(head.x + x,head.y + y);
            ctx.stroke();

            ctx.beginPath();
            x = 40 * Math.cos(this.getAngle());
            y = 40 * Math.sin(this.getAngle());
            ctx.moveTo(head.x + x,head.y + y);
            ctx.lineTo(head.x, head.y);
            ctx.stroke();

            var font_size = 20;
            ctx.fillStyle = canvas.baseColour.getContrast().toString();
            ctx.font= font_size+"px Georgia";
            var disp_angle = this.getAngle()/(2*Math.PI)*360
            ctx.fillText(this.toString());
        }
    };
    
    var circle = function(canvas,p,r) {
        this.canvas = canvas;
        this.type = "circle"; // I expect to have different shapes in future
        this.centre = p;
        this.r = r;
        this.name = name;
        this.extents = [];  // A square outline extent of object
        this.extents.low = new myApp.point(this.centre.x-r, this.centre.y-r);
        this.extents.high = new myApp.point(this.centre.x+r, this.centre.y+r);
        this.start_r = r;
        this.s = 0;
        this.e = 2*Math.PI;
        this.baseColour = new myApp.utils.colour().setRGB(0,0,0);
        this.targetColour = new myApp.utils.colour().setRGB(0,0,0);
        this.path = "";
        this.runTime = 0;
        this.state = STATE_ACTIVE;
        this.birthTime = 0;
        this.showShadow = false;
        this.path.getMovement = function() {return({x:0, y:0});}; // Default to still!!
        this.frictionAir = 0;
        this.frictionBounce = 0;
        this.lifespan = 0; // 0 = Permanent
        this.fadeOut = 1; // No fade out, otherwise from 0(total fade) to 1
        this.shrink = false; // Shrink over its life
        this.bounded = true; // bounded by canvas?
        this.getColourObject = this; // Default to this 
        this.fillStyle = this.fillStyleColour;
        this.density = 1;
        //this.mass = Math.PI*Math.pow(this.r,2);
        this.mass = 4/3*Math.PI*Math.pow(this.r,3);
        this.annotate = false;
        this.gravityForce = null;
        this.coulombs = 0; 
        this.electroForce = null;
        this.visible = true;
        this.collided = false;
        this.timeStep = 0;
        
        console.log("Just created circle: ",this);
        
    };
     // This is not working?? - I Know why - I need to change creation of prototype to 
     // add rather than create prototype from scratch
    //inheritsFrom(circle, simulationObject);
    
    circle.prototype = {
        
        toString:function() {
            var hdr = "Id="+this.id+
                    ",at="+this.centre+
                    "r="+this.r+
                    ":mass="+showNumber(this.getMass(),3)+
                    ":energy="+showNumber(this.getEnergy(),3)+
                    ":speed="+this.path.velocity.toString();
            
            if (this.gravityForce !== null) 
                return (hdr  + ", gravity is "+Math.round(this.gravityForce.x_force)+
                                           ","+Math.round(this.gravityForce.y_force)+
                                           ","+Math.atan(this.gravityForce.y_force / this.gravityForce.x_force) /
                                              (Math.PI * 2) *360);
            else
                return (hdr);
        },
        seed:function(time) {
            this.birthTime = time;
            this.state = STATE_SEED;
        },
        setId:function (id) {
            this.id = id;    
        },
        setName:function(name) {
            this.name = name;
        },
        isActive:function() {
            return (this.state === STATE_ACTIVE)
        },
        setFillStyle:function (fs) {
            this.fillStyle = fs;
        },
        getFillStyle:function () {
            if (typeof(this.fillStyle) === 'function')
                return (this.fillStyle(this.canvas, this));
            else
                return(this.fillStyle);
        },
        // We use an object to get the colour. The idea is that when you 
        // want to use a straight colour for the fillstyle, you set the object
        // and then use this as the fillStyle method
        fillStyleColour:function(d_canvas, d_this) {
            //console.log("Get colour objects is:");
            //console.log(this.getColourObject);
            return(this.getColourObject.getColour(this).toString());
        },
        setBaseColour:function (c) {
            this.baseColour = c;
        },
        getBaseColour:function () {
            return(this.baseColour.toString());
        },
        setTargetColour:function (c) {
            this.targetColour = c;
        },
        getTargetColour:function () {
            return(this.targetColour.toString());
        },
        setGetColourObject:function (obj) {
            this.getColourObject = obj;
        },
        getColour:function (canvas, obj) {
            return(this.getBaseColour());
        },
        setLifespan:function (ls) {
            this.lifespan = ls;
        },
        setShowShadow:function (f) {
            this.showShadow = f;
        },
        setPath: function(pathObj) {
            this.path = pathObj;
        },
        setDensity: function(density) {
            this.density = density;
        },
        setMass: function(m) {
            this.mass = m;
        },
        getMass: function() {
            return this.mass * this.density;
        },
        getEnergy: function() {
            return (0.5*this.getMass()*Math.pow(this.path.getVelocity(),2));
        },
        getMomentum: function() {
            return this.path.getVelocity() * this.getMass();
        },
        getScaleRadius: function() {
            return (this.r*this.canvas.scalingFactor);            
        },
        setFadeOut: function(global_alpha) {
            this.fadeOut = global_alpha;
        },
        setBounceFriction:function (n) {
            this.frictionBounce = n;
        },  
        setAirFriction:function (n) {
            this.frictionAir = n;
        },  
        setShrink:function (f) {
            this.shrink = f;
        },  
        setCharge:function (c) {
            this.coulombs = c;
        },  
        getCharge:function () {
            return(this.coulombs);
        },  
        setBounded:function (f) {
            this.bounded = f;
        },  
        setVisible:function (f) {
            this.visible = f;
        },
        setAnnotate:function(f) {
            this.annotate = f;
        },
        showAnnotation:function (canvas) {
            if ((this.gravityForce !== null) && (this.gravityForce.scalar > 0)) 
                this.drawVector(this.gravityForce);
            else if ((this.electroForce !== null) && (this.electroForce.scalar > 0))
                this.drawVector(this.electroForce);

            var font_size = Math.min(30,Math.max(15,this.getScaledRadius()));

            canvas.ctx.fillStyle = canvas.baseColour.getContrast().toString();
            canvas.ctx.font= font_size+"px Georgia";
            //canvas.ctx.fillText("Speed:"+Math.round(this.path.xSpeed)+"."+
            //                             Math.round(this.path.ySpeed),
            //                     this.x+this.r, this.y+this.r);                    
            //canvas.ctx.fillText("Gravity:"+Math.round(this.gravityForce.x_force)+"."+
            //                             Math.round(this.gravityForce.y_force),
            //                     this.x+this.r, this.y+this.r-font_size);                    
        },
        within: function (point) {
            // Is the give point within this object?
            // For a circle - is distance from our centre < our radius?
            return (this.centre.distance(point) <= this.r);
        },

        drawVector: function(vector) {
            // First we find starting point on circumference, it will be the 
            // pointy end of the vector
            var x = this.x + Math.cos(vector.getAngle()) * this.getScaledRadius();        
            var y = this.y + Math.cos(vector.getAngle()) * this.getScaledRadius(); 
            var pointy_end = new myApp.point(x,y);
            vector.draw(new myApp.point(this.canvas, pointy_end));            
        },
        
        // Can we calculate the gravitational effect?
        calcGravity: function (G) {

            var gravity_vectors = [];
            var g_force;
            var gf_v;
            var g_angle;
            var other_objs = this.canvas.getOtherObjs();

            for (var i = 0; i<other_objs.length; i++) {
                var obj = other_objs[i];
                if (obj.id !== this.id && obj.isActive()) {
                    
                    g_force = G * this.getMass() * obj.getMass() / 
                              Math.pow(this.centre.distance(obj.centre),2);
                    if (g_force > 0) {
                        g_angle = this.centre.getAngle(obj.centre);                        
                        gf_v = new myApp.vector().setScalar(g_force, g_angle);
                        console.log(gf_v);
                        document.getElementById("msg4").innerHTML = "G="+this.path.G+","+g_force+","+this.getMass();                
                        //gravity_vectors.push({scalar:g_force, x:this.x-obj.x, y:this.y - obj.y});
                        gravity_vectors.push(gf_v);
                    }
                }                     
            }
            this.gravityForce = myApp.utils.sumVectors(gravity_vectors);
            
            return (this.gravityForce);
        },        
        // Can we calculate a simple electromagnetic effect?
        calcElectroForce: function () {

            var e_vectors = [];
            var e_force;
            var e_angle;
            var ef_v;
            var other_objs = this.canvas.getOtherObjs();

            for (var i = 0; i<other_objs.length; i++) {
                var obj = other_objs[i];
                if (obj.id !== this.id && obj.isActive()) {
                    e_angle = this.centre.getAngle(obj.centre);                        
                    e_force = myApp.utils.k * this.getCharge() * obj.getCharge() /
                              Math.pow(this.centre.distance(obj.centre),2);
                    ef_v = new myApp.vector().setScalar(e_force, e_angle);
                    e_vectors.push(ef_v);
                }                     
            }
            this.electroForce = myApp.utils.sumVectors(e_vectors);
            document.getElementById("msg4").innerHTML = "Calc ef "+this.x+","+this.y+","+obj.x+","+obj.y+
                                                        ","+this.electroForce.scalar;
            
            return (this.electroForce);
        },        
        beyondBounds:function (x,y) {
            return ((x < 0 - this.r) ||  // Left?
                    (x > this.canvas.width + this.r) ||  // Right?
                    (y > this.canvas.height + this.r) || // bottom?
                    (y < 0 - this.r)); 
        },
        checkExpired: function (runtime) {
            if (this.lifespan > 0)
                return (this.runTime >= this.lifespan);             
            else
                return (false);
        },
        
        checkGermination: function (sim_runtime) {
            return (sim_runtime >= this.birthTime);
        },
        setGlobalAlpha: function(canvas) {
            // Can only fade out if we have a limited life
            if (this.lifespan > 0) {
                var fraction_expired = Math.max(0,1-this.runTime/this.lifespan);
                canvas.ctx.globalAlpha = (1-this.fadeOut) * fraction_expired;      
            }
        },
        // Return amount of life remaining as a percentage (0->1)
        getLifeRemaining: function () {
            if (this.lifespan > 0)
                return (Math.max(0,1-this.runTime/this.lifespan));
            else
                return(1);            
        },
        calcShrinkage: function () {
            // Can only shrink if we have a limited life
            if (this.lifespan > 0) {
                this.r = this.start_r * this.getLifeRemaining();      
            }            
        },
        die: function () {
            
        },
        // Our next position is not clear, so this will manage the collision
        manageCollisions: function(collisions) {

            // Idea is to have the ability to bounce off according to the 
            // different object types. For now, I will only handle a single 
            // circle to circle collision
            var o = collisions[0];
            if (o.type === 'circle') {
                //console.log("Manage Collision ",this,o);
                
                //console.log("B:this:",this.path.velocity.toString());
                //console.log("B:o   :",o.path.velocity.toString());
                
                var msg = "Momentum before: ("+this.canvas.simulation.getTotalMomentum()+") "+
                                               this.id+":"+this.getMomentum().toFixed(0)+";"+
                                               o.id+":"+o.getMomentum().toFixed(0)+";";
                this.canvas.simulation.logMsg(msg);
                var this_v = calcCollisionVelocity(this, o);
                var o_v = calcCollisionVelocity(o, this);
                this.path.velocity.setX(this_v.x);
                this.path.velocity.setY(this_v.y);                        
                o.path.velocity.setX(o_v.x);
                o.path.velocity.setY(o_v.y);
                
                // Now check if we will collide again mid cycle
                // Is the distance from the next position less thatn the combined radii?
                //if (o.centre.distance(this.path.getNextPos(this.timeStep) < this.r + o.r) 
                
                //console.log("A:this:",this.path.velocity.toString());
                //console.log("A:o   :",o.path.velocity.toString());
                        
                var msg = "Momentum after: ("+this.canvas.simulation.getTotalMomentum()+") "+
                                               this.id+":"+this.getMomentum().toFixed(0)+";"+
                                               o.id+":"+o.getMomentum().toFixed(0)+";";
                this.canvas.simulation.logMsg(msg);                
                //o.collided = true;

                return;
            }
            
        },
        // Is the given position within this object?
        checkCollisions: function (x,y) {

            var other_objs = this.canvas.getOtherObjs();
            for (var i = 0; i<other_objs.length; i++) {
                var obj = other_objs[i];
                if (obj.id !== this.id)                     
                    if (myApp.utils.pointsDistance(x,y,obj.x,obj.y)<(this.r+other_objs[i].r)) {
                        return (obj);
                    }
            }
            return (false);
        },        
        checkBoundary:function (next_pos) {

            var collisions = {left: false, right: false, top:false, bottom:false}; //1 = collision
            c_width = this.canvas.width;
            c_height = this.canvas.height;
            
            // Have we hit far side?
            if (next_pos.x + this.r > c_width) {
                collisions.right = true;
                collisions.x_rebound = next_pos.x-(c_width-this.r);
            }
            // Or, have we hit near side?
            else if (next_pos.x < this.r) {
                collisions.left = true;
                collisions.x_rebound = this.r - next_pos.x;
            }

            // Have we hit the bottom      
            if (next_pos.y > c_height - this.r) {
                collisions.bottom = true;
                collisions.y_rebound = next_pos.y - (c_height - this.r);
            }
            // Have we hit the top?
            else if (next_pos.y < this.r) { 
                collisions.top = true;
                collisions.y_rebound = this.r - next_pos.y;
            }

            return (collisions);            
        },
                
        draw:function (canvas) {
            if (this.visible) {
                if (this.state === STATE_ACTIVE) {
                    var save_ga = canvas.ctx.globalAlpha;
                    if (this.fadeOut < 1)
                        this.setGlobalAlpha(canvas);
                    if (this.shrink)
                        this.calcShrinkage();
                    canvas.ctx.beginPath();
                    canvas.ctx.arc(Math.round(this.centre.x),Math.round(this.centre.y),this.r*canvas.scalingFactor,this.s,this.e);
                    canvas.ctx.strokeStyle = this.getBaseColour(); 
                    canvas.ctx.stroke();
                    canvas.ctx.closePath();
                    canvas.ctx.fillStyle = this.getFillStyle(); 
                    canvas.configureShadow(this);
                    //console.log("ctx.fs:");
                    //console.log(canvas.ctx.fillStyle);
                    canvas.ctx.fill();
                    canvas.ctx.globalAlpha = save_ga;

                    if (this.annotate) {
                        this.showAnnotation(canvas);
                    }
                }
            }
        },

        cycle:function (ignore_canvas, i_time) {

            this.timeStep = i_time;
            // If we have collided we skip a cycle to prevent unnecessary collisions, 
            // and prevent the need for a complex collision handler!!
            if (!this.collided) {
                // A collision can require a recycle of the object
                this.recycle = false;

                var last_pos = this.centre;

                this.runTime = this.runTime + i_time;

                // Determine next x pos, change path if we hit wall
                var next_pos = this.path.getNextPos(i_time);

                // Does the next position collide with any other object, if we are colliding
                if (this.getMass() > 0) { // Only check objects that have mass
                    var collisions = this.canvas.checkCollisions(this, next_pos);
                    if (collisions.length > 0) {
                        // To play with, we swap speeds, physics be damned
                        // We only deal with first collision at this point. Next step is
                        // to handle more than one. I was thinking that we share the collision 
                        // based on momentum.
                        this.manageCollisions(collisions);

                        // We leave it where it was. Not best, but simplest at this point
                        //next_pos = this.path.getNextPos(i_time);
                        next_pos = this.centre;
                    }

                    // What about boundary collisions?
                    this.centre = this.canvas.checkBoundaryBounce(this, next_pos);

                    // Have we hit escape velocity - bit of a pressure valve. Some simulations 
                    // can really get objects moving. We went to let them go!!
                    //document.getElementById("msg1").innerHTML = "EV "+this.canvas.escapeVelocity+","+this.path.scalarSpeed;                
                    if (this.path.getVelocity() > this.canvas.escapeVelocity) 
                        this.state = STATE_EXPIRED;                
                }
                else
                    this.centre = next_pos;
                if (this.recycle)
                    this.canvas.simulation.logMsg("About to recycle "+this.id);
            }
            
            this.collided = false;
        }
    };
          
    // This needs to be retested and reworked
    var explosion = function(x,y,r,n) {
        
        this.x = x; // Where to start - x coord
        this.y = y; //                  y coord
        this.r = r; // si
        this.n = n; // Number of particles
        this.speedLower=-1000;
        this.speedUpper=1000;
        this.speedRange = this.speedUpper - this.speedLower;
    };
    
    explosion.prototype = {
        detonate:function(simulation, particles) {
            for (var i = 0; i < particles; i++) {
                y_speed = this.speedRange/2-Math.random()*this.speedRange;
                x_speed = this.speedRange/2-Math.random()*this.speedRange;
                var par = new myApp.parabolic(simulation.canvas,this.x,this.y,this.r,x_speed);
                par.initialise(y_speed);
                par.setAirFriction(.2);
                par.setBounceFriction(.2);
                par.setLifespan(1000+Math.floor(Math.random()*1000));
                par.setColour("grey");
                par.setBounded(Math.random()*10>9);
                simulation.add(par);
            }
        }
    };
    
    var canvas = function(sim, id) {
        
        this.simulation = sim;
        this.id = id;
        this.canvas = document.getElementById(id);
        this.ctx = this.canvas.getContext("2d");
        this.baseColour = new myApp.utils.colour().setRGB(0,0,0);;
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.extents = [];  // A square outline extent of object
        this.extents.low = {x:0, y:0};
        this.extents.high = {x:this.width, y:this.height};
        this.fillStyle = "";
        this.sx = 0;
        this.sy = 0;
        this.showShadow = false;
        this.shadowHasFocus = false;
        this.scalingFactor = 1;
        this.escapeVelocity = Infinity;
        
    };
    canvas.prototype = {
        setBaseColour:function (c) {
            this.baseColour = c;
        },
        getBaseColour:function () {
            return(this.baseColour.toString());
        },
        setFillStyle:function(fs) {
            this.fillStyle = fs;
        },
        setShadowFocus:function(x, y) {
            this.sx = x;
            this.sy = y;
            this.shadowHasFocus = true;
        },
        setScalingFactor:function (n) {
            this.scalingFactor = n;
        },
        setEscapeVelocity:function (n) {
            this.escapeVelocity = n;
        },
        getOtherObjs:function() {
            return (this.simulation.getObjs());
        },
        configureShadow:function(obj) {
            if (obj.showShadow) {
                this.ctx.shadowBlur=10;
                // Is shadow relative to a focus?
                if (this.shadowHasFocus) {
                    this.ctx.shadowOffsetX=(obj.x - this.sx)/this.width*40;
                    this.ctx.shadowOffsetY=(obj.y - this.sy)/this.height*40;
                } 
                else {
                    // Shadow is constant but relative to dimension of canvas
                    this.ctx.shadowOffsetX=this.width/(this.width+this.height)*20;
                    this.ctx.shadowOffsetY=this.height/(this.width+this.height)*20;;                    
                }
                this.ctx.shadowColor="black";                
            } 
            else {
                this.ctx.shadowOffsetX=0;
                this.ctx.shadowOffsetY=0;                
            }                               
        },

        // Does the new point for a give object collide with other objects?
        checkCollisions: function (obj, next_pos) {

            // In future need to handle way more complex checking than this!!
            var coll_objects = [];
            var o;
            
            for (var i = 0; i<this.simulation.objs.length; i++) {
                o = this.simulation.objs[i];
                // Do not check self or objects with no mass
                if ((o.id !== obj.id) && (o.getMass() > 0) && o.isActive()) {
                    // Is distance between centres more than radii combined?
                    if (o.centre.distance(next_pos) < o.r + obj.r) {
                        //console.log("Collided:"+obj.id+"("+obj.centre.toString()+"->"+next_pos.toString()+")"+
                        //        " with "+o.id+"("+o.centre.toString()+")"); 
                        coll_objects.push(o);
                    }
                }
            }
            return (coll_objects);
        },        
        
        checkBoundaryBounce: function (obj, next_pos) {

            if (obj.bounded) {

                c_width = this.width;
                c_height = this.height;

                // Have we hit far side?
                var boundary_collision = obj.checkBoundary(next_pos);
                if (boundary_collision.right) {
                    obj.path.velocity.reflect(Math.PI/2);
                    //obj.path.velocity.x = 0 - obj.path.velocity.x;
                    next_pos.x -= boundary_collision.x_rebound*2;
                }
                // Or, have we hit near side?
                else if (boundary_collision.left) {
                    obj.path.velocity.reflect(Math.PI/2);
                    //obj.path.velocity.x = 0 - obj.path.velocity.x;
                    next_pos.x += boundary_collision.x_rebound*2;
                }

                // Have we hit the bottom      
                if (boundary_collision.bottom) {
                    obj.path.velocity.reflect(0);
                    //obj.path.velocity.y = 0 - obj.path.velocity.y;
                    next_pos.y -= boundary_collision.y_rebound*2;
                }
                // Have we hit the top?
                else if (boundary_collision.top) {
                    obj.path.velocity.reflect(0);
                    //obj.path.velocity.y = 0 - obj.path.velocity.y;
                    next_pos.y += boundary_collision.y_rebound*2;    
                }        

            }

            return (next_pos);            
        },

        draw:function (objs) {
            document.getElementById('locations').innerHTML = "";
            this.ctx.rect(0,0,this.width, this.height);
            this.ctx.fillStyle = this.fillStyle;
            this.ctx.fill();
            
            for (var i = 0; i < objs.length; i++) {
                // Reset shadow before each object
                this.ctx.shadowOffsetX=0;
                this.ctx.shadowOffsetY=0;                
                objs[i].draw(this);
                document.getElementById('locations').innerHTML += "<br>"+objs[i].toString();
            }
        }
    };
    
    var simulation = function(id) {
        this.canvas = new myApp.canvas(this, id);
        this.runTime = 0;    
        this.lifeSpan = -1;
        this.stepping = false; // Are we stepping or running continuous?
        this.objs = [];
        this.numChildren = 0;
        this.expired = 0;
        this.timeStep = 1/60*1000; // Time cycles are roughly 60 per second, 
        this.totalEnergy = 0;
        this.maxEnergy = 0;
        this.minEnergy = null;     
        
    };
    
    simulation.prototype = {
        getCanvas:function () {
            return (this.canvas);
        },
        getObjs:function () {
            return (this.objs);
        },
        add:function (obj) {
            obj.setId(this.numChildren++);            
            this.objs.push(obj);
            document.getElementById("msg2").innerHTML = "Last object created was "+this.numChildren;
        },
        setLifespan:function (n) {
            this.lifeSpan = n;
        },
        setStepMode:function() {
            this.stepping = true;
        },
        setContinuousMode:function() {
            this.stepping = false;
        },
        setTimeStep: function (t) {
            this.timeStep = t;
        },
        restartAnimation:function(animateFunction) {
            animateFunction(this);
        },
        logMsg: function (msg) {
            document.getElementById('log').innerHTML = msg + "\n" + document.getElementById('log').innerHTML;
        },
        getTotalMomentum: function() {
        
            var total = 0;
            for (var i=0; i<this.objs.length; i++) {
                total += this.objs[i].getMomentum();
            }
            
            return total;
        },
        calcEnergy: function () {
            
            var total_energy = 0;
            
            for (var i=0; i<this.objs.length; i++) {
                total_energy += this.objs[i].getEnergy();
            }
            
            this.totalEnergy = total_energy;
            this.maxEnergy = Math.max(this.maxEnergy, this.totalEnergy);
            if (this.minEnergy === null)
                this.minEnergy = this.totalEnergy;
            else
                this.minEnergy = Math.min(this.minEnergy, this.totalEnergy);

            return (total_energy); 
        },
        cycle:function () {
          
            this.performCycle(this.timeStep);
                
            // Calc the energy in the system
            this.calcEnergy();
            document.getElementById('energy').innerHTML = "Energy: min="+this.minEnergy.toExponential(3)+
                                                                 ";max="+this.maxEnergy.toExponential(3)+
                                                                 ";current="+this.totalEnergy.toExponential(3);
        },
        step:function () {
            this.performCycle(this.timeStep);
        },
        
        performCycle:function (time) {
            
            this.runTime += time;
            //console.log("Sim.cycle");
            
            // Cycle each object
            for (var i = 0; i < this.objs.length; i++) {
                switch (this.objs[i].state) {
                    case STATE_SEED:
                        if (this.objs[i].checkGermination(this.runTime)) {
                            this.objs[i].state = STATE_ACTIVE;
                            console.log("Activated "+this.objs[i].birthTime+","+this.runTime);
                            console.log(this.objs[i]);
                            this.objs[i].cycle(this.canvas, time);
                        }
                        break;
                    case STATE_ACTIVE:
                        if (this.objs[i].checkExpired(this.runTime)) 
                            this.objs[i].state = STATE_EXPIRED;
                        else
                            this.objs[i].cycle(this.canvas, time);
                        break;
                    case STATE_STOPPED:
                        break;
                    case STATE_EXPIRED: 
                        obj = this.objs[i];
                        obj.die(this);
                        this.objs.splice(i,1);                        
                        break;
                };
            }
            
            // Now draw objects
            this.canvas.draw(this.objs);
        }        
    };
    
    return {explosion: explosion,
            point: point, 
            vector: vector,
            circle: circle,
            canvas: canvas,
            simulation: simulation
            };
})();
         


function animate (simulation) {

    simulation.cycle();

    // request new frame
    requestAnimFrame(function() {
        if (keep_animating)  {
          animate(simulation);
        }
    });
}
  